const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 4000;

const userRoutes = require("./routes/userRoutes.js")
const productRoutes = require("./routes/productRoutes.js")
const orderRoutes = require("./routes/orderRoutes.js")


const cors = require("cors")

mongoose.connect('mongodb+srv://jomaxbag:ck-star2@batch139.m1ksa.mongodb.net/capstone-e-commerce?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors())

// app.use("/api/courses", courseRoutes)
app.use("/api/users", userRoutes)
app.use("/api/products", productRoutes)
app.use("/api/orders", orderRoutes)

app.listen(PORT, () => console.log(`Server running at port ${PORT}`))
