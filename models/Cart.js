const mongoose = require("mongoose")
const cartSchema = new mongoose.Schema({

	totalAmount:{
		type: Number,
		required: [true]
	},

	order:{
		
		product:{
			type: String
		},

		price:{
			type: Number
		},

		purchasedOn:{
			type: Date,
			default: new Date()
		},
	}
	
})

module.exports = mongoose.model("Cart", cartSchema)