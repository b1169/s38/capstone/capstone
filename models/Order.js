const mongoose = require("mongoose")
const orderSchema = new mongoose.Schema({

	price:{
		type: Number
	},

	purchasedOn:{
		type: Date,
		default: new Date()
	},

	userID:{
			type: String,
			required: [true, "User ID is required"]
	},

	product:{
			type: String,
			required: [true, "Product ID is required"]
	}
	
})

module.exports = mongoose.model("Order", orderSchema)