const mongoose = require("mongoose")
const userSchema = new mongoose.Schema({

	email:{
		type: String,
		required: [true, "Email is required"]
	},

	password:{
		type: String,
		required: [true, "Email is required"]
	},

	isAdmin:{
		type: Boolean,
		default: false
	},

	orders:[{
		product:{
			type: String
		},

		price:{
			type: Number
		},

		purchasedOn:{
			type: Date,
			default: new Date()
		},

		status:{
			type: String,
			default: "ordered"
		}
	}]
})

module.exports = mongoose.model("User", userSchema)