const Product = require("./../models/Product.js")
const bcrypt = require("bcrypt")
const auth = require("./../auth")

module.exports.getActiveProducts = (reqBody) => {
	return Product.find({isActive:true}).then((result=>{
		return result
	}))
}

module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({

		product: reqBody.product,
		description:reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((result, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.getSpecificProduct = (reqBody) => {

	return Product.findOne({product: reqBody}).then((result, error) => {

		console.log(result)
		if(result === null){
			
			return `Product not existing`
		} else{

			if(result){

				return result
			}
			else{
				return error
			}
			
		}
	})
}

module.exports.archiveProduct = (params) => {

	return Product.findByIdAndUpdate(params, {isActive: false}).then((result, error) => {

		if(result === null){
			return `Product not existing`
		}else{
			if(result){
				return true
			}else{
				return false
			}
		}
	})
}


module.exports.updateProduct = (id, reqBody) => {
	const {product, description, price} = reqBody

	let updatedProduct = {
		product: product,
		description: description,
		price: price
	}
	return Product.findByIdAndUpdate(id, updatedProduct, {new:true}).then((result, error) => {

			console.log(result)
			if(error){
				return false
			}else{
				return true
			}

	})
}