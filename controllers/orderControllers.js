const Order = require("./../models/Order.js")
const User = require("./../models/User.js")
const Product = require("./../models/Product.js")
const Cart = require("./../models/Cart.js")
const bcrypt = require("bcrypt")
const auth = require("./../auth")
const authAdmin = require("./../authAdmin")
const authNotAdmin = require("./../authNotAdmin")


module.exports.createOrder = (data) => {

	return Product.findOne({product: data.product}).then((result,error) => {

		if(result == null){

			return `Product Not Found`

		}

		else{

			let newOrder = new Order({

				product: data.product,
				userID: data.userID,
				price: data.price
			})

			return newOrder.save().then((result, error) => {

				if(error){
					return false
				}else{
					return true
				}
			})
			
		}

	})
}

module.exports.addOrders = async (data) => {

	const {userID, product, price} = data;

	let checker = true;

	const productOrder = await Product.findOne({product: data.product}).then((result, error) => {


		if(result == null){

			checker = false

		}
	})
	
	const userOrder = await User.findById(userID).then((result, error) => {

		console.log(checker)
		if(checker == true){

			if(error){
			return error
		}

		else{

			result.orders.push({product: product, price:price})
				return result.save().then((result, error) => {
					if(error){
						return error
					}else{
						return true
					}
				})
		}
		}

		})

}

module.exports.checkOrders = (data) => {

	const {userID, product} = data

	return Order.find(product).then((result=>{
		return result
	}))
}
