const User = require("./../models/User.js")
const Order = require("./../models/Order.js")
const Cart = require("./../models/Cart.js")
const bcrypt = require("bcrypt")
const auth = require("./../auth")

module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody 

	return User.findOne({email: email}).then( (result, error) => {
		
		if(result != null){
			return `Email is already registered.`
		} 
		
		else {
		
			if(result == null){
				return true
			} 

			else {
				return error 
			}
		}
	})
}


module.exports.register= (reqBody) =>{

	return User.findOne({email: reqBody.email}).then((result,error) => {

		if(result != null){
			return `Email is already registered.`
		}

		else{

			let newUser = new User({

				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((result, error) =>{
				
				if(result){
					return true
				}

				else{
					return error
				}
			})
		}
	})
	
}

module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} 

		else {
			return error
		}
	})
}


module.exports.login = (reqBody) => {
	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} 

		else {
		
			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} 

			else {
				return false
			}
		}
	})
}

module.exports.setUserAsAdmin = (params) => {

	return User.findByIdAndUpdate(params, {isAdmin: true}).then((result, error) => {

		console.log(error)
		if(error){
			return `Action cannot be done.`
		}else{
			if(result){
				return true
			}else{
				return false
			}
		}
	})
}

module.exports.getAllAdmin = () => {

	return User.find({isAdmin:true}).then( (result, error) => {
		if(result){
			return result
		} 

		else {
			return error
		}
	})
}

module.exports.createOrder = async (data) => {
	const {userID, product} = data

	const userOrder = await User.findById(userID).then((result, error) => {

		if(error){
			return error
		}

		else{
			result.orders.push({product: product})

			return result.save().then((result, error) => {
					if(error){
						return error
					}else{
						return true
					}
			})
		}
	})
}

module.exports.checkOrders = () => {

	const arrayResult = [];
	
	return User.find().then( (result, error) => {

		for(let i=0; i<result.length; i++){

			if(result[i].orders.length != 0){
				arrayResult.push(result[i])
			}
		}

		return arrayResult
		
	})
}

module.exports.checkMyOrders = (data) => {
	
	let j = 0;
	return User.find().then( (result, error) => {
		
		for(let i=0; i<result.length; i++){
			if(data.userID == result[i].id){
				j = i
			}
		}

		if(data.userID == result[j].id){
			return result[j].orders
		}

		else{
			return "You are not allowed to do so."
		}

	})
}

module.exports.totalAmountOrders = (data) => {

	let totalAmount =0;
	const {userID} = data;
	const arrayResult = [];

	return User.findById(userID).then((result, error) => {

		for(let i=0; i<result.orders.length; i++){

			totalAmount += result.orders[i].price;
		}

		arrayResult.push({totalAmount: totalAmount});

		return arrayResult;
	})
}