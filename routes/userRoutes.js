const express = require("express")
const router = express.Router()
const auth = require("./../auth")
const authAdmin = require("./../authAdmin")
const authNotAdmin = require("./../authNotAdmin")
const userController = require("./../controllers/userControllers.js")
const orderController = require("./../controllers/orderControllers.js")

router.post("/email-exists", (req, res) => {

	userController.checkEmail(req.body).then( result => res.send(result))
})

router.post("/register", (req, res) =>{

	userController.register(req.body).then(result => res.send(result))

})

router.get("/", (req, res) => {

	userController.getAllUsers().then( result => res.send(result))
})

router.post("/login", (req, res) => {

	userController.login(req.body).then(result => res.send(result))
})

router.put("/:userID/set-as-admin", authAdmin.verify, (req, res) => {
	
	userController.setUserAsAdmin(req.params.userID).then(result => res.send(result))

})

router.get("/admin", authAdmin.verify, (req, res) => {

	userController.getAllAdmin().then( result => res.send(result))
})

/*router.post("/checkout1", auth.verify, authNotAdmin.verify, (req,res) =>{

	let data = {
		userID: auth.decode(req.headers.authorization).id,
		product: req.body.product
	}

	userController.createOrder(data).then(result => res.send(result))
})

router.get("/orders1", auth.verify, authAdmin.verify, (req, res) => {

	userController.checkOrders().then( result => res.send(result))
})*/

router.get("/myOrders", auth.verify, authNotAdmin.verify, (req, res) => {


	let data = {
		userID: auth.decode(req.headers.authorization).id
	} 

	userController.checkMyOrders(data).then( result => res.send(result))
})

router.get("/totalAmount", auth.verify, authNotAdmin.verify, (req, res) => {


	let data = {
		userID: auth.decode(req.headers.authorization).id
	} 

	userController.totalAmountOrders(data).then( result => res.send(result))
})

router.post("/checkout", auth.verify, authNotAdmin.verify, (req,res) =>{

	let data = {
		userID: auth.decode(req.headers.authorization).id,
		product: req.body.product,
		price: req.body.price
	}

	orderController.createOrder(data).then(result => res.send(result))
	orderController.addOrders(data).then(result => res.send(result))
})

router.get("/orders", auth.verify, authAdmin.verify, (req, res) => {

	let data = {
		userID: auth.decode(req.headers.authorization).id,
		product: req.body.product,
		price: req.body.price
	}

	orderController.checkOrders(data).then( result => res.send(result))

})

module.exports = router;