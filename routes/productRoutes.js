const express = require("express")
const router = express.Router()
const auth = require("./../auth")
const authAdmin = require("./../authAdmin")
const productController = require("./../controllers/productControllers.js")

router.get("/active-products", (req,res) =>{
	productController.getActiveProducts(req.body.isActive).then(result => res.send(result))
})

router.post("/create-products", auth.verify, authAdmin.verify,(req, res) => {

	productController.createProduct(req.body).then(result => res.send(result))
})


router.get("/specific-product", (req, res) => {

	productController.getSpecificProduct(req.body.product).then( result => res.send(result))
})

router.put("/:productID/archive", auth.verify, authAdmin.verify,(req, res) => {

	productController.archiveProduct(req.params.productID).then(result => res.send(result))
})

router.put("/:productID/update", auth.verify, authAdmin.verify, (req, res) => {

	productController.updateProduct(req.params.productID, req.body).then(result => res.send(result))
})

module.exports = router;