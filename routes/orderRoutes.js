const express = require("express")
const router = express.Router()
const auth = require("./../auth")
const authAdmin = require("./../authAdmin")
const authNotAdmin = require("./../authNotAdmin")

const orderController = require("./../controllers/orderControllers.js")



module.exports = router;